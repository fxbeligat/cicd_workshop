SHELL := /bin/bash
.DEFAULT_GOAL := help
.PHONY: help

ORG ?= francois-xavier.beligat
APP ?= cicd_workshop
VERSION ?= $(shell git describe --always) #? determine the application version

help: # display help message
	@python3 /usr/local/bin/make-help-helper.py < $(MAKEFILE_LIST)

deploy:
	@kubectl get pod

build: BUILDER := img
build: ## build the software
	@echo "building..."
	img build -t $(ORG)/$(APP):$(VERSION) .

push: BUILDER := img
push: ## push the image
	
test: unittest e2etest ## run the tests
	@echo "tests"

unittest: HOST := localhost
unittest: PORT := 80
unittest: ## run the tests 
	@echo "unit tests"
	curl -s $(HOST):$(PORT) | grep -qc "background: green;"
	curl -s $(HOST):$(PORT) | grep -qc "background: red;" || echo "Ok, not red"
	curl -s $(HOST):$(PORT) | grep -qc "background: purple;" || echo "Ok, not purple"
	curl -s $(HOST):$(PORT) | grep -qc "background: black;" || echo "Ok, not black"

e2etest: ## run e2e tests
	@echo "end to end tests"
